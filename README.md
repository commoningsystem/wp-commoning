# commoning wordpress theme

- https://commoningsystem.org/

## Deployment

- [Link to the Registry](https://gitlab.com/commoningsystem/wp-commoning/-/packages)

deployments are built when a user tags a realese it could be smarter but for now:

- before tagging read on which files should contain the new version number
- if you tag a commit (e.g. 1.0.4)
- and push it to gitlab.com
- the .gitlab CI generates a new `commoning.zip` and uploads it to the generic registry
- [Link to the Registry](https://gitlab.com/commoningsystem/wp-commoning/-/packages)

### manualy setting the version number inside project files:

you should alter the version info in those files (to stay consistent) and to let the Wordpress and the admin know it is
an update (or downgrade)

- wp-content/themes/commoning/src/sass/style.scss (__hugely important__)
    - look for and update: `Version: 0.11.0`
      - this is the info wordpress reads in the theme install menu
- wp-content/themes/commoning/functions.php (__also important__)
    - look for and update `define( '_S_VERSION', '0.11.0' );`
        - this is used to tag the assets and make sure the users get the updated ones, and not their cached ones
- wp-content/themes/commoning/package.json
- wp-content/themes/commoning/package-lock.json
- package.json
- package-lock.json

### Installation

- it is important that the zip is named `commoning.zip` before you upload it

## Development

- based on [underscores theme](https://underscores.me/)
- modded to support [tailwindcss](https://tailwindcss.com/)
- find theme in wp-content/themes/commoning
- development setup is based on https://github.com/visiblevc/wordpress-starter.git
    - added
        - xdebug
        - composer
        - nodejs
    - see .docker_env-development/wordpress-starter.dockerfile

### shortcuts

- http://localhost:8080 # wordpress
- http://localhost:50100 # browsesync (autoreload on code change)
- http://localhost:50110 # browsersync UI
- http://localhost:50022 # phpmyadmin

### startup

```sh
## this will initiate docker-containers
docker-compose up -d

## if you want to see some progress
docker-compose logs -f wordpress
docker-compose logs -f browsersync 

## this will update translations
docker-compose exec wordpress sh update-translations.sh

```

**Note 1**: after successful launch of `docker-compose up -d` you'll find the plugin 'advanced custom fields' in a newly
created `plugins` folder. if you don't want that, remove or comment out the corresponding line in `docker-compose.yml` (
line 26)

**Note 2**: on every startup `npm install` will run in the node container, and `npm run watch`  in the browsersync
container (which might fail at first run)
and `composer install` will run in the composer container, they will use the theme directory as their target

### login

#### wordpress backend

- http://localhost:8080/wp-admin

```bash
user: admin
pass: admin
```

#### phpmyadmin

- http://localhost:50022

```bash
user: root
pass: root
```

### composer

```sh
docker-compose run composer composer install
docker-compose run composer composer lint:php
docker-compose run composer composer lint:wpcs
```

##### available commands

taken from [underscore doc](https://github.com/automattic/_s#available-cli-commands)

* composer lint:wpcs : checks all PHP files against PHP Coding Standards.
* composer lint:php : checks all PHP files for syntax errors.
* composer make-pot : generates a .pot file in the languages/ directory.

### demo content

- .testdata/themeunittestdata.wordpress.xml
- https://github.com/WPTT/theme-unit-test
