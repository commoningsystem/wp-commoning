<?php
require_once( get_template_directory() . '/inc/class-tgm-plugin-activation.php' );
require_once( get_template_directory() . '/inc/commoning-theme-options.php' );
if ( getenv( 'ENV_TYPE' ) === 'dev' ) {
	add_filter( 'redirect_canonical', '__return_false' );
}
/**
 * commoning functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package commoning
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '0.13.0' );
}

if ( ! function_exists( 'commoning_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function commoning_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on commoning, use a find and replace
		 * to change 'commoning' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'commoning', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-header'                => __( 'Primary (Header)', 'commoning' ),
				'menu-footer'                => __( 'Secondary (Footer)', 'commoning' ),
				'menu-footer-external-links' => __( 'External Links (Footer)', 'commoning' ),
			)
		);

		/**
		 * Add styling classes which can be defined in wp_nav_menu
		 * on the <li> element of a wordpress menu
		 *
		 * @param $classes
		 * @param $item
		 *
		 * @return mixed
		 */
		function tailwind_nav_class( $classes, $item, $args ) {
			if ( 'menu-header' === $args->theme_location ) {
				$classes[] = '';
				if ( in_array( 'current-menu-item', $classes ) ) {
					$classes[] = 'lg:bg-black text-black underline lg:no-underline lg:text-white lg:font-bold text-base2';
				} else {
					$classes[] = 'lg:hover:bg-black lg:hover:text-white hover:underline bg-white text-black text-base2';
				}
			}
			if ( 'menu-footer' === $args->theme_location ) {
				$classes[] = '';
				if ( in_array( 'current-menu-item', $classes ) ) {
					$classes[] = 'text-white font-bold underline';
				} else {
					$classes[] = 'text-white hover:underline';

				}
			}
			if ( isset( $args->add_li_class ) ) {
				$classes[] = $args->add_li_class;
			}

			return $classes;
		}

		add_filter( 'nav_menu_css_class', 'tailwind_nav_class', 1, 3 );

		/**
		 * Check menu markup for indicators
		 * that it is the primary menu
		 *
		 * @param $html
		 *
		 * @return false|int
		 */
		function is_header_menu( $html ) {
			$primary_menu_identification = "id=\"menu-header\"";

			return strpos( $html, $primary_menu_identification );
		}

		/**
		 * Add css classes to the <a> tag of every menu item
		 *
		 * @param $html
		 *
		 * @return string|string[]|null
		 */
		function add_class_to_menu_item( $html ) {
			if ( is_header_menu( $html ) ) {
				$classes = "block h-full pb-4 lg:pb-8 pt-6 text-center flex justify-center ";

				return preg_replace( '/<a /', "<a class=\"$classes\" ", $html );
			} else {
				return $html;
			}

		}


		add_filter( 'wp_nav_menu', 'add_class_to_menu_item' );


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
//                'search-form',
//                'comment-form',
//                'comment-list',
//                'gallery',
//                'caption',
//                'style',
//                'script',
			)
		);

		// Set up the WordPress core custom background feature.
		/*
		add_theme_support(
			'custom-background',
			apply_filters(
				'commoning_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);
		*/

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'commoning_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function commoning_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'commoning_content_width', 640 );
}

add_action( 'after_setup_theme', 'commoning_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function commoning_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'commoning' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'commoning' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

//add_action( 'widgets_init', 'commoning_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function commoning_scripts() {
	wp_enqueue_style( 'commoning-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'commoning-style', 'rtl', 'replace' );

	wp_enqueue_script( 'commoning-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	wp_register_script( 'commoning-sitescript', get_template_directory_uri() . '/js/app.js', array(), _S_VERSION, true );
	$translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
	wp_localize_script( 'commoning-sitescript', 'commoningTheme', $translation_array );
	wp_enqueue_script( 'commoning-sitescript' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'commoning_scripts' );

// @see https://github.com/liabru/matter-js/issues/196
function commoning_load_template_scripts() {
	if ( is_page_template( 'page-aktiv.php' ) ) {
		wp_enqueue_script( 'pathseg', get_template_directory_uri() . '/js/pathseg.js', array(), _S_VERSION, true );
	}
}

add_action( 'wp_enqueue_scripts', 'commoning_load_template_scripts' );


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/acf-custom-fields.php';

/**
 * Remove trimming of excerpt markup
 * because wie want to keep strong on page
 *
 * @return string
 */
function commoning_allowedtags() {
	// Add custom tags to this string
	return '<h2>,<h1>,<h3>,<h4>,,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>, <strong>';
}

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
if ( ! function_exists( 'commoning_custom_wp_trim_excerpt' ) ) :

	function commoning_custom_wp_trim_excerpt( $wpse_excerpt ) {
		$raw_excerpt = $wpse_excerpt;
		if ( '' == $wpse_excerpt ) {

			$wpse_excerpt = get_the_content( '' );
			$wpse_excerpt = strip_shortcodes( $wpse_excerpt );
			$wpse_excerpt = apply_filters( 'the_content', $wpse_excerpt );
			$wpse_excerpt = str_replace( ']]>', ']]&gt;', $wpse_excerpt );
			$wpse_excerpt = strip_tags( $wpse_excerpt, commoning_allowedtags() ); /*IF you need to allow just certain tags. Delete if all tags are allowed */

			//Set the excerpt word count and only break after sentence is complete.
			$excerpt_word_count = 120;
			$excerpt_length     = apply_filters( 'excerpt_length', $excerpt_word_count );
			$tokens             = array();
			$excerptOutput      = '';
			$count              = 0;

			// Divide the string into tokens; HTML tags, or words, followed by any whitespace
			preg_match_all( '/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens );

			foreach ( $tokens[0] as $token ) {

				if ( $count >= $excerpt_length && preg_match( '/[\,\;\?\.\!]\s*$/uS', $token ) ) {
					// Limit reached, continue until , ; ? . or ! occur at the end
					$excerptOutput .= trim( $token );
					break;
				}

				// Add words to complete sentence
				$count ++;

				// Append what's left of the token
				$excerptOutput .= $token;
			}

			$wpse_excerpt = trim( force_balance_tags( $excerptOutput ) );

			$excerpt_end  = ' <a href="' . esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf( __( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title() ) . '</a>';
			$excerpt_more = apply_filters( 'excerpt_more', ' ' . $excerpt_end );

			//$pos = strrpos($wpse_excerpt, '</');
			//if ($pos !== false)
			// Inside last HTML tag
			//$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
			//else
			// After the content
			$wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

			return $wpse_excerpt;

		}

		return apply_filters( 'commoning_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt );
	}

endif;

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
add_filter( 'get_the_excerpt', 'commoning_custom_wp_trim_excerpt' );

/**
 * Changing excerpt more
 *
 * @param $more
 *
 * @return string
 */
function commoning_excerpt_more( $more ) {
	global $post;
	remove_filter( 'excerpt_more', 'new_excerpt_more' );

	return '';
}

add_filter( 'excerpt_more', 'commoning_excerpt_more' );

/**
 * Add a 'Add data-target="modal-form" to link' checkbox to the WordPress link editor
 *
 * @see https://danielbachhuber.com/tip/rel-nofollow-link-modal/
 */
add_action( 'after_wp_tiny_mce', function () {
	?>
    <script>
        var originalWpLink;
        // Ensure both TinyMCE, underscores and wpLink are initialized
        if (typeof tinymce !== 'undefined' && typeof _ !== 'undefined' && typeof wpLink !== 'undefined') {
            // Ensure the #link-options div is present, because it's where we're appending our checkbox.
            if (tinymce.$('#link-options').length) {
                // Append our checkbox HTML to the #link-options div, which is already present in the DOM.
                tinymce.$('#link-options').append(<?php echo json_encode( '<div class="data-target-modal-from"><label><span></span><input type="checkbox" id="wp-data-target-modal-form"  /> öffne das Kontaktformular in Modal</label></div>' ); ?>);
                // Clone the original wpLink object so we retain access to some functions.
                originalWpLink = _.clone(wpLink);
                wpLink.addDataTarget = tinymce.$('#wp-data-target-modal-form');
                // Override the original wpLink object to include our custom functions.
                wpLink = _.extend(wpLink, {
                    /**
                     * Fetch attributes for the generated link based on
                     * the link editor form properties.
                     *
                     * In this case, we're calling the original getAttrs()
                     * function, and then including our own behavior.
                     */
                    getAttrs: function () {
                        var attrs = originalWpLink.getAttrs();
                        wpLink.correctURL();
                        attrs['data-target'] = wpLink.addDataTarget.prop('checked') ? 'modal-form' : false;
                        return attrs;
                    },

                    /**
                     * Set the value of our checkbox based on the presence
                     * of the rel='nofollow' link attribute.
                     *
                     * In this case, we're calling the original mceRefresh()
                     * function, then including our own behavior
                     */
                    mceRefresh: function (searchStr, text) {
                        originalWpLink.mceRefresh(searchStr, text);
                        var editor = window.tinymce.get(window.wpActiveEditor)
                        if (typeof editor !== 'undefined' && !editor.isHidden()) {
                            var linkNode = editor.dom.getParent(editor.selection.getNode(), 'a[href]');
                            if (linkNode) {
                                wpLink.addDataTarget.prop('checked', 'data-target' === editor.dom.getAttrib(linkNode, 'data'));
                            }
                        }
                    }
                });
            }
        }
    </script>
    <style>
        #wp-link #link-options .data-target-modal-form {
            padding: 3px 0 0;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        #wp-link #link-options .data-target-modal-form label span {
            width: 83px;
        }

        .has-text-field #wp-link .query-results {
            top: 223px;
        }
    </style>
	<?php
} );

add_action( 'tgmpa_register', 'commoning_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function commoning_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(


		// This is an example of how to include a plugin bundled with a theme.
		array(
			// The plugin name.
			'name'               => 'Advanced Custom Fields',
			// The plugin slug (typically the folder name).
			'slug'               => 'advanced-custom-fields',
			// The plugin source.
			'source'             => 'https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.5.zip',
			// If false, the plugin is only 'recommended' instead of required.
			'required'           => true,
			// E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'version'            => '5.9.5',
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_activation'   => true,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'force_deactivation' => false,
			// If set, overrides default API URL and points to an external URL.
			'external_url'       => 'https://downloads.wordpress.org/plugin/advanced-custom-fields.5.9.5.zip',
			// If set, this callable will be be checked for availability to determine if a plugin is active.
			'is_callable'        => '',
		),

		array(
			'name'     => 'Poly Lang',
			'slug'     => 'polylang',
			'required' => true,
		),
		array(
			'name'     => 'Disable Gutenberg',
			'slug'     => 'disable-gutenberg',
			'required' => true,
		),
		array(
			'name'     => 'Contact Form 7', // The plugin name.
			'slug'     => 'contact-form-7', // The plugin slug (typically the folder name).
			'required' => true, // If false, the plugin is only 'recommended' instead of required.
		),


		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		// Unique ID for hashing notices for multiple instances of TGMPA.
		'id'           => 'commoning',
		// Default absolute path to bundled plugins.
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'capability'   => 'edit_theme_options',
		// Show admin notices or not.
		'has_notices'  => true,
		// If false, a user cannot dismiss the nag message.
		'dismissable'  => true,
		// If 'dismissable' is false, this message will be output at top of nag.
		'dismiss_msg'  => '',
		// Automatically activate plugins after installation or not.
		'is_automatic' => false,
		// Message to output right before the plugins table.
		'message'      => '',
		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'commoning' ),
			'menu_title'                      => __( 'Install Plugins', 'commoning' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'commoning' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'commoning' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'commoning' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'commoning'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'commoning'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'commoning'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'commoning'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'commoning'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'commoning'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'commoning'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'commoning'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'commoning'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'commoning' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'commoning' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'commoning' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'commoning' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'commoning' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'commoning' ),
			'dismiss'                         => __( 'Dismiss this notice', 'commoning' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'commoning' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'commoning' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}

add_filter( 'body_class', 'commoning_debug_body_class' );
function commoning_debug_body_class( $classes ) {
	if ( getenv( 'ENV_TYPE' ) === 'dev' ) {
		$classes[] = 'debug-screens';
	}

	return $classes;

}