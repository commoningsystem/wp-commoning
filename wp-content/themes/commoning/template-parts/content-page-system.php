<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package commoning
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content text-sm lg:text-base 2xl:text-lg pl-2 sm:p-8 2xl:p-16 leading-loose text-white links-default-underlined">
        <div>
            <?php the_content(); ?>
        </div>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
