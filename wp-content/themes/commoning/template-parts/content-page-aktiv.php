<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package commoning
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content text-2sm pl-24 p-12 md:pl-24 lg:p-36 xl:text-2sm 2xl:text-lg p-2 lg:p-8 2xl:p-32 leading-loose text-white links-default-underlined overflow-y-hidden">
        <div class="relative ">
            <div class="absolute top-96 -left-18 svg transform scale-170 sm:scale-160 sm:top-7 md:scale-100 md:top-4 md:-left-52 lg:scale-110 lg:top-40 lg:-left-44 2xl:scale-160 2xl:top-96 3xl:scale-125 3xl:top-60">
				<?php get_template_part( 'svg/bullet', 'du-moechtest-sein.svg' ) ?>
            </div>
			<?php the_content(); ?>
        </div>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
