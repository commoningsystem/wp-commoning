<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package commoning
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div id="#primary" class="entry-content text-sm p-8 leading-loose 2xl:text-base 2xl:p-16 prose-dark max-w-none">
        <div>
			<?php the_excerpt(); ?>
            <a id="show-more" class="block mt-12 cursor-pointer group block" href="#show-more">
				<?php get_template_part( 'svg/arrow', 'down.svg' ) ?>
            </a>
        </div>
        <div id="more" class="hidden">
			<?php the_content( false, true ); ?>
            <a id="show-less" class="mt-12 cursor-pointer group block" href="#show-more">
				<?php get_template_part( 'svg/arrow', 'up.svg' ) ?>
            </a>
        </div>
    </div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer">
			<?php
			/*
			edit_post_link(
				sprintf(
					wp_kses(
					/ * translators: %s: Name of current post. Only visible to screen readers * /
						__('Edit <span class="screen-reader-text">%s</span>', 'commoning'),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post(get_the_title())
				),
				'<span class="edit-link">',
				'</span>'
			);
			*/
			?>
        </footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
