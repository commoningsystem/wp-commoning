module.exports = {
    purge: {
        content: [
            '*.php',
            './**/*.php',
        ],
        options: {
            safelist: {
                standard: [/^has-/, /^align/, /^wp-/, /^type/]
            }
        }
    },
    theme: {
        rotate: {
            '-180': '-180deg',
            '-140': '-140deg',
            '-160': '-160deg',
            '-90': '-90deg',
            '-45': '-45deg',
            '-12': '-12deg',
            '-6': '-6deg',
            '-3': '-3deg',
            '-2': '-2deg',
            '-1': '-1deg',
            0: '0deg',
            1: '1deg',
            2: '2deg',
            3: '3deg',
            6: '6deg',
            12: '12deg',
            45: '45deg',
            90: '90deg',
            140: '140deg',
            160: '160deg',
            180: '180deg',
        },
        scale: {
            0: '0',
            50: '.5',
            75: '.75',
            80: '.8',
            90: '.9',
            95: '.95',
            100: '1',
            105: '1.05',
            110: '1.1',
            125: '1.25',
            150: '1.5',
            160: '1.6',
            170: '1.7',
            175: '1.75',
            200: '2',
        },
        screens: {
            'sm': '640px',
            // => @media (min-width: 640px) { ... }

            'md': '768px',
            // => @media (min-width: 768px) { ... }

            'lg': '1024px',
            // => @media (min-width: 1024px) { ... }

            'xl': '1280px',
            // => @media (min-width: 1280px) { ... }

            '2xl': '1441px',
            // => @media (min-width: 1441px) { ... }

            '3xl': '1920px',
            // => @media (min-width: 1920px) { ... }
        },
        fontSize: {
            xs: '0.875rem',
            sm: '1.125rem',
            '2sm': '1.315rem',
            base: '1.5rem', //~28px
            'base2': '1.75rem', //~28px
            lg: '1.875rem',
            xl: '2rem',
            '2xl': '2.25rem',
            '3xl': '2.5rem',
            '4xl': '2.75rem',
            '5xl': '3rem',
            '6xl': '4rem',
        },
        inset: {
            '0': '0',
            '1': '1rem',
            '2': '2rem',
            '3': '3rem',
            '4': '4rem',
            '5': '5rem',
            '6': '6rem',
            '7': '7rem',
            '8': '8rem',
            '9': '9rem',
            '40': '10rem',
            '60': '15rem',
            '72': '18rem',
            '96': '24rem',
            '28': '7rem',
            '-1': '-1rem',
            '-2': '-2rem',
            '-3': '-3rem',
            '-4': '-4rem',
            '-5': '-5rem',
            '-6': '-6rem',
            '-7': '-7rem',
            '-8': '-8rem',
            '-9': '-9rem',
            '-10': '-10rem',
            '-11': '-11rem',
            '-44': '-11rem',
            '-12': '-12rem',
            '-13': '-13rem',
            '-52': '-13rem',
            '-56': '-14rem',
            '-15': '-15rem',
            '-60': '-15rem',
            '-16': '-16rem',
            '-18': '-18rem',
            '-64': '-16rem',
            '1/3': '33.333%',
            '-1/3': '-33.333%',
            '2/3': '66.666%',
            '-2/3': '-66.666%',
            '1/4': '25%',
            '3/4': '75%',
            '1/2': '50%',
            "1/1": '10%',
            "15/1": '15%',
            "2/1": '20%',
            "3/1": '30%',
            "4/1": '40%',
            "5/1": '50%',
            "6/1": '60%',
            "7/1": '70%',
            "8/1": '80%',
            "9/1": '90%',
            "95/1": '95%',
            '-1/4': '-25%',
            '-3/4': '-75%',
            '-1/2': '-50%',
            "-1/1": '-10%',
            "-2/1": '-20%',
            "-3/1": '-30%',
            "-4/1": '-40%',
            "-5/1": '-50%',
            "-6/1": '-60%',
            "-7/1": '-70%',
            "-8/1": '-80%',
            "-9/1": '-90%',
            "36": '36%',
            "65": '65%',
            "100": '100%'
        },
        aspectRatio: {
            none: 0,
            square: [1, 1],
            "16/9": [16, 9],
            "4/3": [4, 3],
            "21/9": [21, 9]
        },
        stroke: theme => ({
            current: 'currentColor',
            'white': theme('colors.white'),
            'gelb': theme('colors.gelb'),
        }),
        fill: theme => ({
            current: 'currentColor',
            'black': 'black',
            'gelb': theme('colors.gelb'),
            'gruen': theme('colors.gruen'),
            'dunkelgruen': theme('colors.dunkelgruen'),
        }),
        fontFamily:
            {
                sans:
                    "Nunito, Avenir, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji",
                display:
                    "Poppins, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji"
            },
        extend: {
            colors: {
                gelb: "rgb(255,241,0)",
                blau: "rgb(24,52,129)",
                gruen: "rgb(133,219,21)",
                dunkelgruen: "rgb(37,98,70)"
            },
            width: {
                '100': '30rem',
                '112': '35rem',
                '128': '48rem',
            },
            height: {
                '128': '32rem',
                '137': '37',
                '142': '42rem',
                '192': '48rem',
                '200': '50rem',
                '256': '64rem'
            },
            transitionProperty: {
                'height': 'height',
            },
            typography: (theme) => ({
                dark: {
                    css: {
                        color: theme('colors.white'),
                        a: {
                            color: theme('colors.white'),
                            '&:hover': {
                                color: theme('colors.white'),
                            },
                        },

                        h1: {
                            color: theme('colors.white'),
                        },
                        h2: {
                            color: theme('colors.white'),
                        },
                        h3: {
                            color: theme('colors.white'),
                        },
                        h4: {
                            color: theme('colors.white'),
                        },
                        h5: {
                            color: theme('colors.white'),
                        },
                        h6: {
                            color: theme('colors.white'),
                        },

                        strong: {
                            color: theme('colors.white'),
                        },

                        code: {
                            color: theme('colors.white'),
                        },

                        figcaption: {
                            color: theme('colors.white'),
                        },
                    },
                },
            }),
        },
    },
    variants: {
        aspectRatio: ['responsive'],
        margin: ['group-hover', 'responsive'],
        textColor: ['responsive', 'hover', 'focus', 'active', 'group-hover', 'group-active'],
        backgroundColor: ['responsive', 'hover', 'focus', 'active', 'group-hover', 'group-active'],
        fill: ['responsive', 'hover', 'focus', 'group-hover', 'active', 'group-active'],
        stroke: ['responsive', 'hover', 'focus', 'group-hover', 'group-active'],
        strokeWidth: ['responsive', 'hover', 'focus', 'group-hover'],
    },
    plugins: [
        require('@tailwindcss/forms'),
        require("tailwindcss-responsive-embed"),
        require('tailwindcss-aspect-ratio'),
        require("tailwindcss-debug-screens"),
        require('tailwindcss-interaction-variants'),
        require('@tailwindcss/typography'),
    ],

}
