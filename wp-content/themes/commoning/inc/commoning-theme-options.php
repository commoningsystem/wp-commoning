<?php
/**
 * Create A Simple Theme Options Panel
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Start Class
if ( ! class_exists( 'Commoning_Theme_Options' ) ) {

	class Commoning_Theme_Options {

		/**
		 * Start things up
		 *
		 * @since 1.0.0
		 */
		public function __construct() {

			// We only need to register the admin panel on the back-end
			if ( is_admin() ) {
				add_action( 'admin_menu', array( 'Commoning_Theme_Options', 'add_admin_menu' ) );
				add_action( 'admin_init', array( 'Commoning_Theme_Options', 'register_settings' ) );
			}

		}

		/**
		 * Returns all theme options
		 *
		 * @since 1.0.0
		 */
		public static function get_theme_options() {
			return get_option( 'theme_options' );
		}

		/**
		 * Returns single theme option
		 *
		 * @since 1.0.0
		 */
		public static function get_theme_option( $id ) {
			$options = self::get_theme_options();
			if ( isset( $options[ $id ] ) ) {
				return $options[ $id ];
			}
		}

		/**
		 * Add sub menu page
		 *
		 * @since 1.0.0
		 */
		public static function add_admin_menu() {
			add_menu_page(
				esc_html__( 'Theme Settings', 'commoning' ),
				esc_html__( 'Theme Settings', 'commoning' ),
				'manage_options',
				'theme-settings',
				array( 'Commoning_Theme_Options', 'create_admin_page' )
			);
		}

		/**
		 * Register a setting and its sanitization callback.
		 *
		 * We are only registering 1 setting so we can store all options in a single option as
		 * an array. You could, however, register a new setting for each option
		 *
		 * @since 1.0.0
		 */
		public static function register_settings() {
			register_setting( 'theme_options', 'theme_options', array( 'Commoning_Theme_Options', 'sanitize' ) );
		}

		/**
		 * Sanitization callback
		 *
		 * @since 1.0.0
		 */
		public static function sanitize( $options ) {

			// If we have options lets sanitize them
			if ( $options ) {
				// Input
				if ( ! empty( $options['contact-form-id-for-modal'] ) ) {
					$options['contact-form-id-for-modal'] = sanitize_text_field( $options['contact-form-id-for-modal'] );
				} else {
					unset( $options['contact-form-id-for-modal'] ); // Remove from options if empty
				}
			}

			// Return sanitized options
			return $options;

		}

		/**
		 * Settings page output
		 *
		 * @since 1.0.0
		 */
		public static function create_admin_page() { ?>

            <div class="wrap">

                <h1><?php esc_html_e( 'Theme Options', 'commoning' ); ?></h1>

                <form method="post" action="options.php">

					<?php settings_fields( 'theme_options' ); ?>

                    <table class="form-table wpex-custom-admin-login-table">

                        // @todo https://gist.github.com/azizultex/f0b1aae10279c9a7233d
                        // make the setting read contact form 7 options dynamically

						<?php // Text input example ?>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e( 'Contact Form 7 ID für Modal', 'commoning' ); ?></th>
                            <td>
								<?php $value = self::get_theme_option( 'contact-form-id-for-modal' ); ?>
								<?php $value = $value ?: "1"; ?>
                                <input type="text" name="theme_options[contact-form-id-for-modal]"
                                       value="<?php echo esc_attr( $value ); ?>">
                            </td>
                        </tr>

                    </table>

					<?php submit_button(); ?>

                </form>

            </div><!-- .wrap -->
		<?php }

	}
}
new Commoning_Theme_Options();

// Helper function to use in your theme to return a theme option value
function commoning_get_theme_option( $id = '' ) {
	return Commoning_Theme_Options::get_theme_option( $id );
}