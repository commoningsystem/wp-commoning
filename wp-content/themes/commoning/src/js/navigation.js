import AJS from "./animate-anything";

/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function() {
	const siteNavigation = document.getElementById( 'site-navigation' );

	// Return early if the navigation don't exist.
	if ( ! siteNavigation ) {
		return;
	}

	const button = siteNavigation.getElementsByTagName( 'button' )[ 0 ];

	// Return early if the button don't exist.
	if ( 'undefined' === typeof button ) {
		return;
	}

	const menu = siteNavigation.getElementsByTagName( 'ul' )[ 0 ];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	if ( ! menu.classList.contains( 'nav-menu' ) ) {
		menu.classList.add( 'nav-menu' );
	}

	// Toggle the .toggled class and the aria-expanded value each time the button is clicked.
	button.addEventListener( 'click', function() {
		siteNavigation.classList.toggle( 'toggled' );

		if ( button.getAttribute( 'aria-expanded' ) === 'true' ) {
			button.setAttribute( 'aria-expanded', 'false' );
		} else {
			button.setAttribute( 'aria-expanded', 'true' );
		}
	} );

	// Remove the .toggled class and set aria-expanded to false when the user clicks outside the navigation.
	document.addEventListener( 'click', function( event ) {
		const isClickInside = siteNavigation.contains( event.target );

		if ( ! isClickInside ) {
			siteNavigation.classList.remove( 'toggled' );
			button.setAttribute( 'aria-expanded', 'false' );
		}
	} );

	// Get all the link elements within the menu.
	const links = menu.getElementsByTagName( 'a' );

	// Get all the link elements with children within the menu.
	const linksWithChildren = menu.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

	// Toggle focus each time a menu link is focused or blurred.
	for ( const link of links ) {
		link.addEventListener( 'focus', toggleFocus, true );
		link.addEventListener( 'blur', toggleFocus, true );
	}

	// Toggle focus each time a menu link with children receive a touch event.
	for ( const link of linksWithChildren ) {
		link.addEventListener( 'touchstart', toggleFocus, false );
	}

	/**
	 * Sets or removes .focus class on an element.
	 */
	function toggleFocus() {
		if ( event.type === 'focus' || event.type === 'blur' ) {
			let self = this;
			// Move up through the ancestors of the current link until we hit .nav-menu.
			while ( ! self.classList.contains( 'nav-menu' ) ) {
				// On li elements toggle the class .focus.
				if ( 'li' === self.tagName.toLowerCase() ) {
					self.classList.toggle( 'focus' );
				}
				self = self.parentNode;
			}
		}

		if ( event.type === 'touchstart' ) {
			const menuItem = this.parentNode;
			event.preventDefault();
			for ( const link of menuItem.parentNode.children ) {
				if ( menuItem !== link ) {
					link.classList.remove( 'focus' );
				}
			}
			menuItem.classList.toggle( 'focus' );
		}
	}
}() );
///////////////////////////////////////////////////////////////////
/////////////////////  HAMBURGER ANIMATION  //////////////////////
/////////////////////////////////////////////////////////////////
(function () {
///Initiation Variables
	const hamburger_icon = document.getElementById("hamburger-icon");
	const topLine = document.getElementById("top-line-3");
	const middleLine = document.getElementById("middle-line-3");
	const bottomLine = document.getElementById("bottom-line-3");
	if (!hamburger_icon) {
		// console.warn(`missing element "icon"`)
		return;
	}
	if (!topLine) {
		// console.warn('missing element "topLine"')
		return;
	}
	if (!middleLine) {
		// console.warn('missing element "middleLine"')
		return;
	}
	if (!bottomLine) {
		// console.warn('missing element "bottomLine"')
		return;
	}
	let state_3 = "menu";  // can be "menu" or "cross"

///Animation Variables
	const segmentDuration_3 = 10;
	const menuDisappearDurationInFrames_3 = segmentDuration_3;
	const crossAppearDurationInFrames_3 = segmentDuration_3 * 1.5;
	const crossDisappearDurationInFrames_3 = segmentDuration_3 * 1.5;
	const menuAppearDurationInFrames_3 = segmentDuration_3;
	let menuDisappearComplete_3 = false;
	let crossAppearComplete_3 = false;
	let crossDisappearComplete_3 = true;
	let menuAppearComplete_3 = true;
	let currentFrame_3 = 0;
	const cPt_3 = {x: 50, y: 50};  // center point
	let tlPt_3 = {x: 30, y: 37};  // top right point
	let trPt_3 = {x: 70, y: 37};  // top left point
	let mlPt_3 = {x: 30, y: 50};  // middle right point
	let mrPt_3 = {x: 70, y: 50};  // middle left point
	const blPt_3 = {x: 30, y: 63};  // bottom right point
	const brPt_3 = {x: 70, y: 63};  // bottom left point

///Position Rotation
	function positionRotation(centerPoint, orbitPoint, angleInRads) {
		const distance = Math.sqrt(Math.pow(orbitPoint.x - centerPoint.x, 2) + Math.pow(orbitPoint.y - centerPoint.y, 2));
		orbitPoint.x = centerPoint.x + Math.cos(angleInRads) * distance;
		orbitPoint.y = centerPoint.y + Math.sin(angleInRads) * distance;
	}

///Menu Disappear
	function menuDisappearAnimation_3() {
		currentFrame_3++;
		if (currentFrame_3 <= menuDisappearDurationInFrames_3) {
			window.requestAnimationFrame(() => {
				const rotation = Math.PI * 0.5;
				//top line
				const tlAng = AJS.easeInBack(3.7179, 3.7179 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const trAng = AJS.easeInBack(5.7068, 5.7068 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, tlPt_3, tlAng);
				positionRotation(cPt_3, trPt_3, trAng);
				topLine.setAttribute("d", "M" + tlPt_3.x + "," + tlPt_3.y + " L" + trPt_3.x + "," + trPt_3.y + " Z");
				//middle line
				const mlAng = AJS.easeInBack(Math.PI, Math.PI + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const mrAng = AJS.easeInBack(0, rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, mlPt_3, mlAng);
				positionRotation(cPt_3, mrPt_3, mrAng);
				middleLine.setAttribute("d", "M" + mlPt_3.x + "," + mlPt_3.y + " L" + mrPt_3.x + "," + mrPt_3.y + " Z");
				//bottom line
				const blAng = AJS.easeInBack(2.5652, 2.5652 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const brAng = AJS.easeInBack(0.5763, 0.5763 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, blPt_3, blAng);
				positionRotation(cPt_3, brPt_3, brAng);
				bottomLine.setAttribute("d", "M" + blPt_3.x + "," + blPt_3.y + " L" + brPt_3.x + "," + brPt_3.y + " Z");
				//recursion
				menuDisappearAnimation_3();
			});
		} else {
			currentFrame_3 = 0;
			menuDisappearComplete_3 = true;
			openMenuAnimation_3();
		}
	}

///Cross Appear
	function crossAppearAnimation_3() {
		currentFrame_3++;
		if (currentFrame_3 <= crossAppearDurationInFrames_3) {
			tlPt_3 = {x: 50, y: 28.7867};
			trPt_3 = {x: 50, y: 71.2132};
			mlPt_3 = {x: 28.7867, y: 50};
			mrPt_3 = {x: 71.2132, y: 50};
			window.requestAnimationFrame(() => {
				const rotation = Math.PI * 0.75;
				//top line
				const tlAng = AJS.easeOutBack(Math.PI, Math.PI + rotation, crossAppearDurationInFrames_3, currentFrame_3);
				const trAng = AJS.easeOutBack(0, rotation, crossAppearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, tlPt_3, tlAng);
				positionRotation(cPt_3, trPt_3, trAng);
				topLine.setAttribute("d", "M" + tlPt_3.x + "," + tlPt_3.y + " L" + trPt_3.x + "," + trPt_3.y + " Z");
				//center line
				const mlAng = AJS.easeOutBack(Math.PI * 1.5, Math.PI * 1.5 + rotation, crossAppearDurationInFrames_3, currentFrame_3);
				const mrAng = AJS.easeOutBack(Math.PI * 0.5, Math.PI * 0.5 + rotation, crossAppearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, mlPt_3, mlAng);
				positionRotation(cPt_3, mrPt_3, mrAng);
				middleLine.setAttribute("d", "M" + mlPt_3.x + "," + mlPt_3.y + " L" + mrPt_3.x + "," + mrPt_3.y + " Z");
				//bottom line
				bottomLine.style.opacity = 0;
				//recursion
				crossAppearAnimation_3();
			});
		} else {
			currentFrame_3 = 0;
			crossAppearComplete_3 = true;
			openMenuAnimation_3();
		}
	}

///Combined Open Menu Animation
	function openMenuAnimation_3() {
		if (!menuDisappearComplete_3) {
			menuDisappearAnimation_3();
		} else if (!crossAppearComplete_3) {
			crossAppearAnimation_3();
		}
	}

///Cross Disappear
	function crossDisappearAnimation_3() {
		currentFrame_3++;
		if (currentFrame_3 <= crossDisappearDurationInFrames_3) {
			window.requestAnimationFrame(() => {
				const rotation = Math.PI * 0.75;
				//top line
				const tlAng = AJS.easeInBack(Math.PI * 1.75, Math.PI * 1.75 + rotation, crossDisappearDurationInFrames_3, currentFrame_3);
				const trAng = AJS.easeInBack(Math.PI * 0.75, Math.PI * 0.75 + rotation, crossDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, tlPt_3, tlAng);
				positionRotation(cPt_3, trPt_3, trAng);
				topLine.setAttribute("d", "M" + tlPt_3.x + "," + tlPt_3.y + " L" + trPt_3.x + "," + trPt_3.y + " Z");
				//center line
				const mlAng = AJS.easeInBack(Math.PI * 2.25, Math.PI * 2.25 + rotation, crossDisappearDurationInFrames_3, currentFrame_3);
				const mrAng = AJS.easeInBack(Math.PI * 1.25, Math.PI * 1.25 + rotation, crossDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, mlPt_3, mlAng);
				positionRotation(cPt_3, mrPt_3, mrAng);
				middleLine.setAttribute("d", "M" + mlPt_3.x + "," + mlPt_3.y + " L" + mrPt_3.x + "," + mrPt_3.y + " Z");
				//bottom line
				bottomLine.style.opacity = 0;
				//recursion
				crossDisappearAnimation_3();
			});
		} else {
			middleLine.style.opacity = "1";
			currentFrame_3 = 0;
			crossDisappearComplete_3 = true;
			closeMenuAnimation_3();
		}
	}

///Menu Appear
	function menuAppearAnimation_3() {
		currentFrame_3++;
		if (currentFrame_3 <= menuAppearDurationInFrames_3) {
			tlPt_3 = {x: 37, y: 70};
			trPt_3 = {x: 37, y: 30};
			mlPt_3 = {x: 50, y: 70};
			mrPt_3 = {x: 50, y: 30};
			bottomLine.style.opacity = 1;
			window.requestAnimationFrame(() => {
				const rotation = Math.PI * 0.5;
				//top line
				const tlAng = AJS.easeOutBack(2.1471, 2.1471 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const trAng = AJS.easeOutBack(4.1360, 4.1360 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, tlPt_3, tlAng);
				positionRotation(cPt_3, trPt_3, trAng);
				topLine.setAttribute("d", "M" + tlPt_3.x + "," + tlPt_3.y + " L" + trPt_3.x + "," + trPt_3.y + " Z");
				//middle line
				const mlAng = AJS.easeOutBack(Math.PI * 0.5, Math.PI * 0.5 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const mrAng = AJS.easeOutBack(Math.PI * 1.5, Math.PI * 1.5 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, mlPt_3, mlAng);
				positionRotation(cPt_3, mrPt_3, mrAng);
				middleLine.setAttribute("d", "M" + mlPt_3.x + "," + mlPt_3.y + " L" + mrPt_3.x + "," + mrPt_3.y + " Z");
				//bottom line
				const blAng = AJS.easeOutBack(0.9944, 0.9944 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				const brAng = AJS.easeOutBack(5.2887, 5.2887 + rotation, menuDisappearDurationInFrames_3, currentFrame_3);
				positionRotation(cPt_3, blPt_3, blAng);
				positionRotation(cPt_3, brPt_3, brAng);
				bottomLine.setAttribute("d", "M" + blPt_3.x + "," + blPt_3.y + " L" + brPt_3.x + "," + brPt_3.y + " Z");
				//recursion
				menuAppearAnimation_3();
			});
		} else {
			currentFrame_3 = 0;
			menuAppearComplete_3 = true;
		}
	}

///Close Menu Animation
	function closeMenuAnimation_3() {
		if (!crossDisappearComplete_3) {
			crossDisappearAnimation_3();
		} else if (!menuAppearComplete_3) {
			menuAppearAnimation_3();
		}
	}

///Events
	hamburger_icon.addEventListener("click", () => {
		if (state_3 === "menu") {
			openMenuAnimation_3();
			state_3 = "cross";
			crossDisappearComplete_3 = false;
			menuAppearComplete_3 = false;
		} else if (state_3 === "cross") {
			closeMenuAnimation_3();
			state_3 = "menu";
			menuDisappearComplete_3 = false;
			crossAppearComplete_3 = false;
		}
	});

}());
