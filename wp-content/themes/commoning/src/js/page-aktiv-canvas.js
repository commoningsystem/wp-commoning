import {Bodies, Body, Common, Composite, Engine, Events, Render, Runner, Svg, Vertices, World} from "matter-js";

// shared variables
let bubble1, bubble2, bubble3, bubble4, bubble5, bubble6;
let canvas, activistsContainer;
let engine, world, render;
let fullWidth, halfWidth;
let fullHeight, halfHeight;
let floorPosition;
const svgPath = commoningTheme.templateUrl + "/svg/bubble-single.svg";
const defaultCategory = 0x0001,
    backgroundCategory = 0x0002;

const xxxlPositions = [];

const WIREFRAME = false;
const OFFSET = 200;
// const GRAVITY = 0.01;
const GRAVITY = 0.5;
const FLOOR_HEIGHT = 20;

function loadBubbleAnimation() {
    canvas = document.getElementById('actors-bg');
    activistsContainer = document.getElementsByClassName('activists');
    if (!canvas) {
        // console.warn('Element not fount, exiting')
        return;
    }
    if (!activistsContainer) {
        // console.warn('Element not fount, exiting');
        return;
    }
    init(canvas, activistsContainer);
    createGround();
    addElementFromSVG(svgPath, 'bubble1', halfWidth - 250, halfHeight - 50 + OFFSET, 1.45);
    addElementFromSVG(svgPath, 'bubble3', halfWidth + 410, halfHeight + 170 + OFFSET, 0.8);
    addElementFromSVG(svgPath, 'bubble4', fullWidth - 320, halfHeight - 200 + OFFSET, 1.1);
    addElementFromSVG(svgPath, 'bubble2', 300, fullHeight - 100 + OFFSET, 1.1);
    addElementFromSVG(svgPath, 'plea', fullWidth - 350, fullHeight - 100 + OFFSET, .6);
    for (let i = 0; i < fullWidth; i += 100) {
        let posY = getRandomArbitrary(-50, 20);
        addBall(i, posY);
    }
}

function addBall(posX, posY = 0) {
    const comp = Composite.create();
    const ball = Bodies.circle(posX, posY, 40, {
        collisionFilter: {
            mask: defaultCategory
        },
    }, 10);
    const comp2 = Composite.add(comp, ball)
    World.add(world, comp2, true);
}

function init(canvas, activistsContainer) {
    const ourCanvas = activistsContainer[0]
    halfWidth = getWidth(ourCanvas) / 2;
    fullWidth = getWidth(ourCanvas);
    fullHeight = getHeight(ourCanvas);
    halfHeight = getHeight(ourCanvas) / 2;
    floorPosition = canvas.height + FLOOR_HEIGHT;
    window.addEventListener("resize", function () {
        render.canvas.width = getWidth(activistsContainer[0]);
        render.canvas.height = getHeight(activistsContainer[0]);
    });
    canvas.width = getWidth(activistsContainer[0]);
    canvas.height = getHeight(activistsContainer[0]);

    engine = Engine.create();
    world = engine.world;
    world.gravity.y = GRAVITY;

    render = Render.create({
        element: canvas,
        engine: engine,
        options: {
            width: canvas.width,
            height: canvas.height + OFFSET,
            wireframes: WIREFRAME,
            background: 'transparent',
        }
    });
    Render.run(render);
    let runner = Runner.create();
    Runner.run(runner, engine);
}

function createGround() {
    const ground = Bodies.rectangle(halfWidth, floorPosition, canvas.width, FLOOR_HEIGHT, {
        isStatic: true,
        render: {
            visible: true
        }
    });
    World.add(world, [
        ground
    ]);
}

/**
 *
 * @param elem
 * @returns {number}
 */
function getWidth(elem) {
    return elem.clientWidth;
}

/**
 *
 * @param elem
 * @returns {number}
 */
function getHeight(elem) {
    return elem.clientHeight;
}

/**
 *
 * @param {string} svgPath
 * @param {number} scale
 * @param {boolean} isStatic
 * @returns {Promise<unknown>}
 */
function getVertexSetFromSVG(svgPath, scale = 1, isStatic = true) {
    if (typeof fetch !== 'undefined') {
        const select = function (root, selector) {
            return Array.prototype.slice.call(root.querySelectorAll(selector));
        };

        const loadSvg = function (url) {
            return fetch(url)
                .then(function (response) {
                    return response.text();
                })
                .then(function (raw) {
                    return (new window.DOMParser()).parseFromString(raw, 'image/svg+xml');
                });
        };
        return new Promise(
            function (resolve, reject) {
                loadSvg(svgPath).then(function (root) {
                        const vertexSets = select(root, 'path')
                            .map(function (path) {
                                return Vertices.scale(Svg.pathToVertices(path, 30), scale, scale);
                            });
                        if (vertexSets) {
                            resolve(vertexSets);
                        } else {
                            reject(Error('could not create vertex set'));
                        }
                    }
                )
            });
    } else {
        Common.warn('Fetch is not available. Could not load SVG.');
    }
}

/**
 *
 * @param {number} min
 * @param {number} max
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 *
 * @param {string} svgPath
 * @param {string} label
 * @param {number} posX
 * @param {number} posY
 * @param {number} scale
 */
function addElementFromSVG(svgPath = "", label = 'bubble1', posX = 0, posY = 0, scale = 1) {
    let vertexSets = false;
    getVertexSetFromSVG(svgPath, scale).then(function (result) {
        if (typeof result !== 'undefined' && result.length > 0) {
            vertexSets = result;
            // console.log(vertexSets);
            const newElement = Bodies.fromVertices(posX, posY, vertexSets, {
                label: label,
                isStatic: true,
                render: {
                    fillStyle: "transparent",
                    strokeStyle: "black",
                    lineWidth: 3
                }
            });
            const offset = 30;
            const newElementShadow = Bodies.fromVertices(posX - offset, posY + offset, vertexSets, {
                label: label + '_shadow',
                isStatic: true,
                collisionFilter: {
                    category: backgroundCategory
                },
                render: {
                    fillStyle: "transparent",
                    strokeStyle: "#CFCFCF",
                    lineWidth: 3
                }
            });
            World.add(world, newElementShadow, true);
            World.add(world, newElement, true);
            const posVariant = getRandomArbitrary(1, 15);
            const timeVariant = getRandomArbitrary(0.01, 0.04);
            // console.log(`posVariant: ${posVariant}`);
            // console.log(`timeVariant: ${timeVariant}`);
            // animateElement(newElement, posX, posY, posVariant, timeVariant);
        }
    });
}

/**
 *
 * @param element
 * @param {number} posX
 * @param {number} posY
 * @param {number} variant
 */
function animateElement(element, posX = 0, posY = 0, variant = 10) {
    let counter = 0;
    Events.on(engine, 'beforeUpdate', function (event) {
        counter += 0;
        let py = posY + variant * Math.sin(engine.timing.timestamp * 0.002);
        Body.setPosition(element, {x: posX, y: py});
    });
}

export {loadBubbleAnimation};