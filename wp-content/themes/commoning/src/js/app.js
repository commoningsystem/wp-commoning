// require('typeface-nunito');
import jump from 'jump.js'
// import AJS from "./animate-anything";
// import {loadBubbleAnimation} from "./page-aktiv-canvas";

/**
 * SHOW MORE
 * KLAPP AUS
 */
(function () {
    const showMore = document.getElementById('show-more');
    const showLess = document.getElementById('show-less');
    const more = document.getElementById('more');
    const unfoldMe = document.getElementById('unfold_me');
    const call_to_action_interessiert = document.getElementById('call_to_action_interessiert');
    const circle = document.getElementById('circle')
    if (!showMore) {
        // console.warn('missing element "showMore"')
        return;
    }
    if (!more) {
        // console.warn('missing element "more"')
        return;
    }
    if (!call_to_action_interessiert) {
        // console.warn('missing element "call_to_action_interessiert"')
        return;
    }
    if (!circle) {
        // console.warn('missing element "circle"')
        return;
    }
    showMore.addEventListener('click', function (event) {
        event.preventDefault();
        unfoldMe.setAttribute('unfolded', 'true')
        more.classList.remove('hidden')
        showMore.classList.add('hidden')
        call_to_action_interessiert.classList.add('lg:hidden');
        jump(more, {
            offset: -120,
        });
        harmonizeContainerHeightWhenUnfolded('bigger');
    });
    showLess.addEventListener('click', function (event) {
        event.preventDefault();
        unfoldMe.setAttribute('unfolded', 'false')
        jump(unfoldMe, {
            offset: -120,
            callback: function () {
                harmonizeContainerHeightWhenUnfolded("smaller");
                call_to_action_interessiert.classList.remove('lg:hidden');
                more.classList.add('hidden')
                showMore.classList.remove('hidden')
            },
        });
    });

}());

/**
 * get the height of the content container
 * and set the height of the image container to be the same
 */
function harmonizeContainerHeightOnFronpage() {
    if (document.body.classList.contains('page-template-page-app')) {
        const contentContainer = document.getElementById('unfold_me');
        const imageContainer = document.getElementById('image_container')

        // exit if the container is unfolded
        if (contentContainer.getAttribute('unfolded') === 'true') {
            return
        }
        /*
        the image Container is hidden on mobile so it would fail here
         */
        if (contentContainer && imageContainer) {
            cleanHeightAttribute(contentContainer);
            // only if it is not unfolded
            imageContainer.style.height = contentContainer.offsetHeight + "px";
        }
    }
}

function cleanHeightAttribute(elem) {
    if (elem.style.removeProperty) {
        elem.style.removeProperty('height');
    } else {
        elem.style.removeAttribute('height')
    }
}

window.addEventListener('resize', harmonizeContainerHeightOnFronpage);
harmonizeContainerHeightOnFronpage();

/**
 *
 * @param direction
 */
function harmonizeContainerHeightWhenUnfolded(direction = "bigger") {
    if (document.body.classList.contains('page-template-page-app')) {
        const contentContainer = document.getElementById('unfold_me');
        const imageContainer = document.getElementById('image_container')
        const leftColumn = document.getElementById('lg_column_left');
        const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
        const widthToggle = 1024 // 'lg' of tailwindcss

        // exit early if viewport is too small
        if (vw < widthToggle) {
            return;
        }

        /*
        the image Container is hidden on mobile so it would fail here
         */
        if (leftColumn && contentContainer && imageContainer) {
            if (direction === 'bigger') {
                contentContainer.style.height = leftColumn.offsetHeight + "px";
            } else if (direction === "smaller") {
                // console.log(`unfolding to ${direction}`);
                contentContainer.style.height = imageContainer.offsetHeight + "px";
            } else {
                // console.warn('no direction given, please provide one to harmonizeConteinerHeightWhenUnfolded')
            }
        }
    }
}

window.onscroll = function () {
    scrollFunction()
};

window.addEventListener("scroll", function () {
    const struktur_light = document.getElementById('struktur-light')
    const struktur_dark = document.getElementById('struktur-dark')

    if (!struktur_light || !struktur_dark) {
        return;
    }
    if (checkvisible(struktur_light)) {
        parralax(struktur_light)
    }
    if (checkvisible(struktur_dark)) {
        parralax(struktur_dark, 'down')
    }
});

function parralax(elem, direction = 'up') {
    let distance = window.scrollY;
    let firstTop = elem.offsetTop;
    let topPos = elem.getBoundingClientRect().top + window.scrollY;
    let theSvg = elem.children[0];
    let moveTop = firstTop * 0.2 //speed;
    let dist2 = (distance - topPos) / 2;
    // console.log(firstTop, distance, topPos, dist2);
    if (direction === "up") {
        theSvg.style.transform = `translateY(${moveTop - dist2}px)`
    }
    if (direction === "down") {
        theSvg.style.transform = `translateY(${moveTop + dist2}px)`
    }

}

function posY(elm) {
    var test = elm, top = 0;

    while (!!test && test.tagName.toLowerCase() !== "body") {
        top += test.offsetTop;
        test = test.offsetParent;
    }

    return top;
}

function viewPortHeight() {
    var de = document.documentElement;

    if (!!window.innerWidth) {
        return window.innerHeight;
    } else if (de && !isNaN(de.clientHeight)) {
        return de.clientHeight;
    }

    return 0;
}

function scrollY() {
    if (window.pageYOffset) {
        return window.pageYOffset;
    }
    return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
}

function checkvisible(elm) {
    var vpH = viewPortHeight(), // Viewport Height
        st = scrollY(), // Scroll Top
        y = posY(elm);

    return (y < (vpH + st));
}

function scrollFunction() {
    const headerLogoBig = document.getElementById('header-logo-image-big');
    const headerLogoBigDesktop = document.getElementById('header-logo-image-big-desktop');
    const headerLogoSmall = document.getElementById('header-logo-image-small');
    const introVideo = document.getElementById('intro-video');
    const siteBranding = document.getElementById('site-branding');


    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        if (document.body.classList.contains('page-template-page-app')) {
            // console.log('scrolled a bit');
            if (headerLogoBigDesktop) {
                headerLogoBigDesktop.classList.remove('lg:block');
                headerLogoBigDesktop.classList.add("lg:hidden");
            }
            if (headerLogoBig) {
                headerLogoBig.classList.add("hidden");
            }
            if (headerLogoSmall) {
                headerLogoSmall.classList.remove('hidden');
            }
            if (siteBranding) {
                siteBranding.classList.remove("lg:h-96")
                siteBranding.classList.add("lg:h-40")
            }
        }
        if (introVideo) {
            // introVideo.classList.remove('w-1/2')
            // introVideo.classList.add('w-3/4')
        }
    } else {
        if (document.body.classList.contains('page-template-page-app')) {
            // console.log('the top of the site');
            if (headerLogoBigDesktop) {
                headerLogoBigDesktop.classList.remove("lg:hidden");
                headerLogoBigDesktop.classList.add('lg:block');
            }
            if (headerLogoBig) {
                headerLogoBig.classList.remove("hidden");
            }
            if (headerLogoSmall) {
                headerLogoSmall.classList.add("hidden");
            }
            if (siteBranding) {
                siteBranding.classList.remove("lg:h-40")
                siteBranding.classList.add("lg:h-96")
            }
        }
        // console.log('scrolled up');
        if (introVideo) {
            // introVideo.classList.add('w-1/2')
            // introVideo.classList.remove('w-3/4')
        }
    }
}

(function () {
    let openmodal = document.querySelectorAll('.modal-open')
    if (!openmodal) {
        console.warn('.modal-open is missing');
        return;
    }
    for (let i = 0; i < openmodal.length; i++) {
        openmodal[i].addEventListener('click', function (event) {
            event.preventDefault()
            toggleModal()
        })
    }

    const overlay = document.querySelector('.modal-overlay')
    if (!overlay) {
        console.warn('.modal-overlay is missing');
        return;
    }
    overlay.addEventListener('click', toggleModal)

    let closemodal = document.querySelectorAll('.modal-close')
    if (!closemodal) {
        console.warn('.modal-close is missing');
        return;
    }
    for (let i = 0; i < closemodal.length; i++) {
        closemodal[i].addEventListener('click', toggleModal)
    }

    document.onkeydown = function (evt) {
        evt = evt || window.event
        let isEscape = false
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc")
        } else {
            isEscape = (evt.keyCode === 27)
        }
        if (isEscape && document.body.classList.contains('modal-active')) {
            toggleModal()
        }
    };


}());

function toggleModal() {
    const body = document.querySelector('body')
    const modal = document.querySelector('.modal')
    modal.classList.toggle('opacity-0')
    modal.classList.toggle('pointer-events-none')
    body.classList.toggle('modal-active')
}

domReady(function () {
    const modalButtonsOnSite = document.querySelectorAll("[data-target='modal-form']");
    if (modalButtonsOnSite.length > 0) {
        modalButtonsOnSite.forEach((button) => {
            button.addEventListener('click', function _openModal(e) {
                e.preventDefault();
                toggleModal();
            });
        })
    }
    // loadBubbleAnimation();
});

function domReady(fn) {
    // If we're early to the party
    document.addEventListener("DOMContentLoaded", fn);
    // If late; I mean on time.
    if (document.readyState === "interactive" || document.readyState === "complete") {
        fn();
    }
}

