<?php
/*
* Template Name: Page: System
* Template Post Type: page
*/

get_header();
?>
    <main id="primary" class="site-main w-full text-2sm font-light lg:font-normal lg:text-base 2xl:text-xl mt-28 lg:mt-40">
		<?php
		/*
		 * Video
		 */
		?>
        <div class="flex flex-col lg:flex-row">
            <div class="w-full">
                <div class="embed-responsive aspect-ratio-16/9">
					<?php if ( get_field( 'video' ) ): ?>
						<?php if ( get_field( 'video' ) ): ?>
							<?php $field = get_field( 'video' ); ?>
							<?php $field = str_replace( '<iframe ', '<iframe class="embed-responsive-item" ', $field ); ?>
							<?php echo( $field ); ?>
						<?php endif; ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
		<?php
		/*
		 * Main Content
		 */
		?>
        <div class="flex flex-col bg-blau prose prose-dark max-w-none lg:prose-lg xl:prose-xl 2xl:prose-2xl">
            <div class="w-full p-4 sm:p-8 sm:pt-16 sm:pb-16 2xl:p-16">
                <div class="w-full lg:w-4/5 2xl:w-2/3">
					<?php if ( get_field( 'subheadline' ) ): ?>
                        <h2 class="text-white text-sm2 lg:text-base2 2xl:text-xl font-bold leading-loose"><?php the_field( 'subheadline' ); ?></h2>
					<?php endif; ?>
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'page-system' );
					endwhile; // End of the loop.
					?>
					<?php if ( get_field( 'note_below_content' ) ): ?>
                        <p class="text-white text-sm lg:text-base 2xl:text-lg font-bold"><?php the_field( 'note_below_content' ); ?></p>
					<?php endif; ?>
                </div>
            </div>
        </div>
		<?php
		/*
		 * Last Row
		 */
		?>
        <div class="flex flex-col lg:flex-row">
			<?php
			/*
			 * Black Box (Hast du Fragen oder Anmerkungen?)
			 */
			?>
            <div class="w-full lg:w-1/2 bg-black text-white system_call_to_action">
				<?php if ( get_field( 'call_to_action_1_headline' ) ): ?>
                    <h2 class="text-sm2 lg:text-base2 p-8 lg:pt-16 lg:pb-4 2xl:w-128"><?php the_field( 'call_to_action_1_headline' ); ?></h2>
				<?php endif; ?>
                <div class="lg:mb-16 call-to-action relative pb-8 pl-8 pt-5 text-sm lg:pl-16 lg:pb-0 xl:pb-8">
                    <div class="w-72 lg:w-full xl:text-base 2xl:text-lg 2xl:w-128 relative">
                        <div class="absolute top-7 -left-56 transform scale-150 md:right-100 lg:top-3 lg:-left-12 lg:scale-125 xl:-left-13 xl:top-5 2xl:-left-11 2xl:top-8 2xl:scale-175 z-0 ">
							<?php get_template_part( 'svg/bullet', 'hast-du-fragen.svg' ) ?>
                        </div>
						<?php if ( get_field( 'call_to_action_1' ) ): ?>
							<?php the_field( 'call_to_action_1' ); ?>
						<?php endif; ?>
                    </div>
                </div>
            </div>
			<?php
			/*
			 * Just some decoration
			 */
			?>
            <div class="w-full lg:w-1/2 lg:relative hidden lg:block overflow-hidden">
                <div class="transform scale-75 relative -bottom-12 -left-12">
                    <div id="struktur-light" class="lg:absolute top-5 left-2 w-full transform rotate-160">
						<?php get_template_part( 'svg/struktur', 'interesse-layer-light.svg' ); ?>
                    </div>
                    <div id="struktur-dark" class="lg:absolute top-3 left-5 w-full transform rotate-160">
						<?php get_template_part( 'svg/struktur', 'interesse-layer-dark.svg' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_sidebar();
get_template_part('template-parts/modal', 'contact-form');
get_footer();
