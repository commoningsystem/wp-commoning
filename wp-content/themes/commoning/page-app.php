<?php
/*
* Template Name: Page: App
* Template Post Type: page
*/
get_header();
?>
    <main id="primary"
          class="site-main w-full text-2sm font-light lg:font-normal lg:text-base 2xl:text-2xl mt-28 md:mt-40 pt-48 md:pt-0">
		<?php // ZEILE 1, text links, video rechts ?>
        <div class="flex flex-col lg:flex-row">
            <div class="w-full lg:w-1/2 bg-blau text-white flex flex-col content-center justify-center pl-8 pr-8"
                 id="intro-text">
				<?php if ( get_field( 'introduction' ) ): ?>
                    <p class="pt-8 pb-8 lg:pt-12 md:pt-20 xl:pt-24 2xl:pt-48">
						<?php the_field( 'introduction' ); ?>
                    </p>
				<?php endif; ?>
            </div>
            <div class="w-full lg:w-1/2 bg-black" id="intro-video">
                <div class="embed-responsive aspect-ratio-square">
					<?php if ( get_field( 'video' ) ): ?>
						<?php $field = get_field( 'video' ); ?>
						<?php $field = str_replace( '<iframe ', '<iframe class="embed-responsive-item" ', $field ); ?>
						<?php echo( $field ); ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
		<?php // ZEILE 2, 2mal call to action ?>
        <div class="flex flex-col md:flex-row bg-gelb py-6 lg:p-16 text-lg lg:text-4xl">
            <div class="w-full lg:w-1/2 flex flex-col">
                <div class="w-full 2xl:mt-8 px-6">
					<?php

					$link                   = get_field( 'call_to_action_1' );
					$link_should_open_modal = ! empty( get_field( 'call_to_action_1_open_contact_form' ) );
					if ( $link ):
						$link_url = $link['url'];
						$link_title         = $link['title'];
						$link_target        = $link['target'] ? $link['target'] : '_self';
						?>
                        <a class="button group active:bg-black active:text-gelb p-2 block lg:inline flex justify-between items-center"
                           href="<?php echo esc_url( $link_url ); ?>"
							<?php if ( $link_should_open_modal ): ?>
                                data-target="modal-form"
							<?php endif ?>
                           target="<?php echo esc_attr( $link_target ); ?>">
                            <span><?php echo esc_html( $link_title ); ?></span>
							<?php get_template_part( 'svg/arrow', 'right.svg' ) ?>
                        </a>
					<?php endif; ?>
                </div>
                <div class="w-full mt-4 mb-4 lg:mt-0 2xl:mt-8 2xl:mb-8 px-6">
					<?php
					$link_2                  = get_field( 'call_to_action_2' );
					$link2_should_open_modal = ! empty( get_field( 'call_to_action_2_open_contact_form' ) );
					if ( $link_2 ):
						$link_url = $link_2['url'];
						$link_title          = $link_2['title'];
						$link_target         = $link_2['target'] ? $link_2['target'] : '_self';
						?>
                        <a class="button group active:bg-black active:text-gelb p-2 block lg:inline flex justify-between items-center"
                           href="<?php echo esc_url( $link_url ); ?>"
							<?php if ( $link2_should_open_modal ): ?>
                                data-target="modal-form"
							<?php endif ?>
                           target="<?php echo esc_attr( $link_target ); ?>">
                        <span>
                        <?php echo esc_html( $link_title ); ?>
                        </span>
                            <span>
						<?php get_template_part( 'svg/arrow', 'right.svg' ) ?>
                        </span>
                        </a>
					<?php endif; ?>
                </div>
            </div>
            <div class="w-full lg:w-1/2">
				<?php if ( get_field( 'crowdfunding_embed_code' ) ): ?>
					<?php the_field( 'crowdfunding_embed_code' ); ?>
				<?php endif ?>
            </div>
        </div>
		<?php // ZEILE 3, links ein Bild, rechts der ausklapp Text ?>
        <div class="flex flex-col lg:flex-row">
            <div class="lg:w-1/2 w-full hidden lg:block" id="lg_column_left">
				<?php // BILDER ?>
                <div class="lg:flex align-center justify-center hidden overflow-hidden "
                     id="image_container">
					<?php
					$image_besides_content = get_field( 'image_besides_content' );
					$size                  = 'large'; // (thumbnail, medium, large, full or custom size)

					if ( ! empty( $image_besides_content ) ): ?>
						<?php echo wp_get_attachment_image( $image_besides_content, $size, false, [ 'class' => 'object-contain transform xl:scale-110 2xl:scale-125 2xl:mt-12 ' ] ); ?>
					<?php endif; ?>
                </div>
				<?php // MOEGLICHKEITEN (Desktop)  ?>
                <div id="howtosupport"
                     class="relative w-full bg-blau text-white pt-12 pb-24 pl-8 pr-4 lg:pt-24 xl:pt-36 xl:pb-36">
					<?php if ( get_field( 'possibilities_headline' ) ): ?>
                        <h2><?php the_field( 'possibilities_headline' ); ?></h2>
					<?php endif; ?>
					<?php // further styling in style:css ?>
                    <div class="relative mt-3 lg:mt-6 " id="possibilities_list_and_decor">
                        <div class="absolute  z-0 possibilities-decoration">
							<?php get_template_part( 'svg/bubble', 'einbringen.svg' ) ?>
                        </div>
                        <div class="ml-12 text-sm relative lg:mt-24 lg:ml-48 lg:text-xl possibilities-list">
							<?php if ( get_field( 'possibilities_list' ) ): ?>
								<?php the_field( 'possibilities_list' ); ?>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
			<?php // CONTENT - UNFOLDS ?>
            <div class="lg:w-1/2 w-full z-10 relative" id="lg_column_right">
                <div class="w-full bg-black text-white z-10 relative" id="unfold_me">
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'page-app' );
					endwhile; // End of the loop.
					?>
                </div>
				<?php /*
				// MOEGLICHKEITEN (mobile)
                ist doppelt drin, weil die Reihenfolge der Element in der Layoutvorlage
                sich in der Handyansicht von der auf dem Deskot unterscheidet und im Rahmen des aufklappens
                des contents nicht anders zu organisieren war. Es gibt sicher einen besesren Weg als den Inhalt
                doppelt in den Quelltext zu packen, aber erstmal so damit es funktioniert
                 */ ?>
                <div id="howtosupport"
                     class="lg:hidden relative w-full bg-blau text-white pt-12 lg:pt-24 pb-24 pl-8 pr-4 h-142">
					<?php if ( get_field( 'possibilities_headline' ) ): ?>
                        <h2><?php the_field( 'possibilities_headline' ); ?></h2>
					<?php endif; ?>
                    <div class="relative mt-3">
                        <div class="ml-12 xl:ml-48 text-sm relative">
							<?php if ( get_field( 'possibilities_list' ) ): ?>
								<?php the_field( 'possibilities_list' ); ?>
							<?php endif; ?>
                        </div>
                        <div class="absolute top-0 right-9/1 lg:right-7/1 z-0 possibilities-decoration">
							<?php get_template_part( 'svg/bubble', 'einbringen.svg' ) ?>
                        </div>
                    </div>
                </div>
				<?php // CALL TO ACTION  ?>
                <div id="call_to_action_interessiert"
                     class="w-full bg-dunkelgruen lg:bg-gray-200 lg:h-full z-10 relative overflow-hidden">
                    <div id="struktur-light" class="hidden lg:block absolute top-2 left-2 w-11/12">
						<?php get_template_part( 'svg/struktur', 'interesse-layer-light.svg' ); ?>
                    </div>
                    <div id="struktur-dark" class="hidden lg:block absolute top-5 left-2 w-11/12">
						<?php get_template_part( 'svg/struktur', 'interesse-layer-dark.svg' ); ?>
                    </div>
                    <div class="content p-8 lg:p-6 z-10 flex items-center justify-between relative lg:top-1/1 2xl:top-1/1 lg:left-4/1 lg:pr-48 text-white text-xl lg:text-3xl 2xl:text-6xl lg:mb-6">
                        <div class="circle absolute z-0 hidden lg:block" id="circle"></div>
                        <div class="z-10">
							<?php if ( get_field( 'call_to_action_3_headline' ) ): ?>
                                <p><?php the_field( 'call_to_action_3_headline' ); ?></p>
							<?php endif; ?>
							<?php
							$link3                   = get_field( 'call_to_action_3' );
							$link3_should_open_modal = ! empty( get_field( 'call_to_action_3_open_contact_form' ) );
							if ( $link3 ):
								$link_url = $link3['url'];
								$link_title          = $link3['title'];
								$link_target         = $link3['target'] ?: '_self';
								?>
                                <a class="button" href="<?php echo esc_url( $link_url ); ?>"
									<?php if ( $link3_should_open_modal ): ?>
                                        data-target="modal-form"
									<?php endif ?>
                                   target="<?php echo esc_attr( $link_target ); ?>">
                                <span>
                                    <?php echo esc_html( $link_title ); ?>
                                </span>
                                    <span class="hidden lg:inline lg:mt-12">
                                    <?php get_template_part( 'svg/arrow', 'right.white.svg' ) ?>
                                </span>
                                </a>
							<?php endif; ?>
                        </div>
                        <div class="">
                            <span>
                             <?php get_template_part( 'svg/arrow', 'right.white.svg' ) ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_sidebar();
get_template_part( 'template-parts/modal', 'contact-form' );
get_footer();
