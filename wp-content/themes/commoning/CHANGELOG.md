# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

__Types of changes__

- __Added__ for new features.
- __Changed__ for changes in existing functionality.
- __Deprecated__ for soon-to-be removed features.
- __Removed__ for now removed features.
- __Fixed__ for any bug fixes.
- __Security__ in case of vulnerabilities.

## [Unreleased]

## [0.13.0] - 2021-06-04

### Added

- [Global] - new theme region in the footer for social links

## [0.12.2] - 2021-05-31

### Fixed

- [Page:Aktiv] Backend Option for teams had typo

## [0.12.1] - 2021-05-31

### Fixed

- [Page:Aktiv] Backend Option for teams is now tinymce

## [0.12.0] - 2021-04-21

### Added

- [Page:Aktiv] Fields for Team members (grid display of li elements)
- [Page:App] Add Option for crowdfunding embed code ()

### Removed

- [Page:Aktiv] Interactive Team presentation

## [0.11.2] - 2021-04-21

### Fixed

- [Page:System] Embed Fallback [Issue #33](https://gitlab.com/commoningsystem/wp-commoning/-/issues/33)

## [0.11.1] - 2021-04-09

### Changed

- [Page:System] Blue Content Box now
  obeys [tailwindcss/typography](https://github.com/tailwindlabs/tailwindcss-typography)

### Fixed

- [Header] Big Logo on mobile was partially covered by
  menu [Issue #23](https://gitlab.com/commoningsystem/wp-commoning/-/issues/23)
- [Page:System] PositionFix for Embed on
  Desktop [Issue #26](https://gitlab.com/commoningsystem/wp-commoning/-/issues/26)
- [Page:System] Deco Fixes [Issue #29](https://gitlab.com/commoningsystem/wp-commoning/-/issues/26)

### Added

- Environment Variable for development Setup to
  display [debug-screens](https://github.com/jorenvanhee/tailwindcss-debug-screens) on localhost and NOT in production

## [0.11.0] - 2021-03-25

### Added

- 80% of expected behaviour and functionality