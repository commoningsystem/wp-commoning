<svg class="text-white fill-current group-hover:text-black group-hover:stroke-white group-hover:stroke-2" width="36" height="31" viewBox="0 0 36 31" xmlns="http://www.w3.org/2000/svg" >
<path d="M17.5 31L35.5 0C31.6667 1.66667 22.7 5 17.5 5C12.3 5 3.66667 1.66667 0 0L17.5 31Z" />
</svg>
