<svg width="364" height="736" viewBox="0 0 364 736" fill="none" xmlns="http://www.w3.org/2000/svg" class="w-1/2" >
<rect width="364" height="736" rx="26" fill="black"/>
<rect x="13" y="14" width="338" height="708" rx="26" fill="white"/>
<path d="M86 0H279C279 0 278.5 41 268.5 41C258.5 41 109.5 41 98 41C86.4999 41 86 0 86 0Z" fill="black"/>
</svg>
