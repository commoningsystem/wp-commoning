<svg class="stroke-current fill-gelb h-8 w-8 stroke-2 inline group-hover:ml-2 group-active:stroke-gelb" stroke-linecap="round" stroke-linejoin="round"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 36 39.5">
    <path d="M33.5,20L2.5,2c1.7,3.8,5,12.8,5,18s-3.3,13.8-5,17.5L33.5,20z"/>
</svg>
