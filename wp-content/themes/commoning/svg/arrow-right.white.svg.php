<svg class="text-white h-8 w-8 lg:h-12 lg:w-12 stroke-current stroke-2 fill-dunkelgruen inline group-hover:ml-2 group-active:text-gelb group-active:fill-current" stroke-linecap="round" stroke-linejoin="round"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     viewBox="0 0 36 39.5">
    <path class="st-white" d="M33.5,20L2.5,2c1.7,3.8,5,12.8,5,18s-3.3,13.8-5,17.5L33.5,20z"/>
</svg>
