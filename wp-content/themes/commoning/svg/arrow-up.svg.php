<svg class="text-black stroke-2 stroke-white fill-current group-hover:text-white group-hover:stroke-white group-hover:stroke-0" width="36" height="31" viewBox="0 0 36 31" xmlns="http://www.w3.org/2000/svg" >
    <path d="M18,0L0,31c3.8-1.7,12.8-5,18-5s13.8,3.3,17.5,5L18,0z"/>
</svg>
