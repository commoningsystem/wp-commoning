<svg xmlns="http://www.w3.org/2000/svg" width="1848.11" height="681.61" viewBox="0 0 1848.11 681.61">
    <g id="Group_176" data-name="Group 176" transform="translate(-70 -553.699)">
        <g id="Group_152" data-name="Group 152" transform="translate(74.944 738.188)">
            <path id="Path_332" data-name="Path 332" d="M151.044,46.372C135.084-10.482,82.928-34.182,41.535-43.139-28.343-58.256-113.8-35.768-145.547,61.59-169.319,134.481-151,263.865-25.919,283.96c83.184,13.365,152.679-18.711,166.275-80.186C154.018,142,163.256,84.082,248.311,49.289c57.307-23.443,183.141,8.88,203.647,94.87" transform="translate(154.496 47.432)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <g id="Group_153" data-name="Group 153" transform="translate(348.096 558.643)">
            <path id="Path_333" data-name="Path 333" d="M35.45,44.276C9.869,48.688-15.662,65.344-27.406,92.934c-22.47,52.792,1.458,101.6,31.217,120.736,29.137,18.734,83.245,33.308,124.867-6.245,38.467-36.553,24.92-97.269-16.648-131.144-34.914-28.453-27.65-75.843-6.453-97.1,34.656-34.747,82.769-32.074,109.5,5.346,20.841,29.176,26.728,58.8,5.346,85.532-7.27,9.089-16.63,17-26.876,21.515" transform="translate(36.261 45.288)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <g id="Group_154" data-name="Group 154" transform="translate(537.388 687.18)">
            <path id="Path_334" data-name="Path 334" d="M66.747,12.89C52.237-10.334,17.079-23.865-23.014-2.482S-80.56,55.726-58.877,103.431c13.365,29.4,61.311,50.784,101.4,37.419C90.639,124.813,89.35,72.061,89.35,72.061" transform="translate(68.273 13.185)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_155" data-name="Group 155" transform="translate(494.896 580.12)">
            <path id="Path_335" data-name="Path 335" d="M4,35.359C-3.908,27.448-13.8-2.217,16.5-26.293c19.866-15.79,46.526-10.347,57.218.344" transform="translate(4.094 36.167)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_156" data-name="Group 156" transform="translate(376.104 672.332)">
            <path id="Path_336" data-name="Path 336" d="M44.845,4.847c-12.4-10.126-39.553-15.722-63.285,0C-37.716,17.616-51.712,39.5-43.445,72.629" transform="translate(45.87 4.957)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_157" data-name="Group 157" transform="translate(239.037 627.085)">
            <path id="Path_337" data-name="Path 337" d="M29.569.118C60-2.8,90.423,21.249,86.387,60.029c-4.195,40.309-28.688,61.958-60.726,59.8-42.68-2.876-65.33-46.384-52.178-78.448" transform="translate(30.245 0.121)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_158" data-name="Group 158" transform="translate(105.933 772.752)">
            <path id="Path_338" data-name="Path 338" d="M4.666,86.121c-13.42-28.332-19.2-94.993,29.52-141.859,57.5-55.313,130.67-22.931,130.67-22.931" transform="translate(4.773 88.089)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_159" data-name="Group 159" transform="translate(416.103 863.967)">
            <path id="Path_339" data-name="Path 339" d="M2.156,77.764S-19.446,14.606,33.509-45.41C73.6-90.849,156.8-91.274,189.143-44.552" transform="translate(2.205 79.541)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_160" data-name="Group 160" transform="translate(242.825 1059.067)">
            <path id="Path_340" data-name="Path 340" d="M2.182,57.076C-19.474-14.96,42.217-72.292,107.524-55.4,139.795-47.061,175.2-3.321,174.97,35.86c-.394,67.733-59.391,79.075-59.391,79.075" transform="translate(2.232 58.381)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_161" data-name="Group 161" transform="translate(193.674 831.247)">
            <path id="Path_341" data-name="Path 341" d="M242.6,119.438C209.162,223.528,42.693,267.329-32.161,156.256-69.013,101.574-73.043,76.171-65.3-4.124c7.214-74.835-43.969-115.592-93.953-117.932-37.061-1.734-78.608,16.606-88.892,59.13" transform="translate(248.145 122.168)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <path id="Path_342" data-name="Path 342" d="M0,336.829H1848.11V-344.527H0Z" transform="translate(70 898.226)" fill="none"/>
        <text id="Global_" data-name="Global " transform="translate(772.388 754.474)" fill="#171410" font-size="165" font-family="Nunito-Light, Nunito" font-weight="300"><tspan x="0" y="0">Global </tspan></text>
        <text id="C" transform="translate(772.388 952.821)" fill="#171410" font-size="164" font-family="Nunito-Light, Nunito" font-weight="300"><tspan x="0" y="0">C</tspan></text>
        <text id="ommoning_" data-name="ommoning " transform="translate(883.192 953.821)" fill="#171410" font-size="165" font-family="Nunito-Light, Nunito" font-weight="300"><tspan x="0" y="0">ommoning </tspan></text>
        <text id="Sy" transform="translate(772.388 1153.169)" fill="#171410" font-size="165" font-family="Nunito-Light, Nunito" font-weight="300" letter-spacing="-0.005em"><tspan x="0" y="0">Sy</tspan></text>
        <text id="st" transform="translate(955.29 1153.169)" fill="#171410" font-size="165" font-family="Nunito-Light, Nunito" font-weight="300" letter-spacing="-0.005em"><tspan x="0" y="0">st</tspan></text>
        <text id="e" transform="translate(1088.188 1152.169)" fill="#171410" font-size="164" font-family="Nunito-Light, Nunito" font-weight="300" letter-spacing="-0.005em"><tspan x="0" y="0">e</tspan></text>
        <text id="m" transform="translate(1175.569 1153.169)" fill="#171410" font-size="165" font-family="Nunito-Light, Nunito" font-weight="300" letter-spacing="-0.005em"><tspan x="0" y="0">m</tspan></text>
    </g>
</svg>


