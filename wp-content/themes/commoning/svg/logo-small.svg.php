<svg xmlns="http://www.w3.org/2000/svg" width="817.922" height="341.613" viewBox="0 0 817.922 341.613">
    <g id="Group_178" data-name="Group 178" transform="translate(-74.576 -1385.967)">
        <g id="Group_164" data-name="Group 164" transform="translate(89.598 1391.989)">
            <path id="Path_343" data-name="Path 343" d="M204.011,32.432C181.424,10.519,151.3-.928,125.444-6.525,56.605-21.428-27.574.738-58.854,96.71-82.269,168.56-64.226,296.1,58.994,315.9c81.944,13.177,146.952-19.312,163.8-79.041,7.415-26.3,12.683-60.552-1.746-97.858-11.374-29.409-46.131-62.813-93.526-60.179-37.587,2.088-75.17,25.719-79.928,72.207" transform="translate(67.668 10.757)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <g id="Group_165" data-name="Group 165" transform="translate(680.79 1391.989)">
            <path id="Path_344" data-name="Path 344" d="M9.754,137.664C-2.395,157.819-7.405,183.759.765,209.1c18.448,57.208,70.71,79.362,107.249,74.034,35.772-5.211,88.363-30.89,94.064-90.8,5.272-55.365-46.966-94.072-102.9-92.286-46.974,1.5-73.7-40.98-71.557-72.364,3.509-51.314,43.232-82.281,89.9-71.063,36.387,8.748,61.307,28.136,62.764,63.981a82.122,82.122,0,0,1-3.621,28.329" transform="translate(3.235 45.662)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <g id="Group_166" data-name="Group 166" transform="translate(742.493 1418.999)">
            <path id="Path_345" data-name="Path 345" d="M4.638,53.979C-.4,45.371-2.974,34.274-.71,20.141c4.79-29.871,30.894-39.049,46-37.961" transform="translate(1.538 17.904)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_167" data-name="Group 167" transform="translate(711.683 1561.312)">
            <path id="Path_346" data-name="Path 346" d="M35.5,0C19.421.51-3.837,15.317-9.956,40.635A62,62,0,0,0-7.511,78.2" transform="translate(11.775)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_168" data-name="Group 168" transform="translate(128.351 1426.502)">
            <path id="Path_347" data-name="Path 347" d="M11.23,138C-8.178,109.207-17.584,30.677,40.045-19.064c65.9-56.875,148.217-5.235,148.217-5.235" transform="translate(3.725 45.774)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_169" data-name="Group 169" transform="translate(427.034 1435.348)">
            <path id="Path_348" data-name="Path 348" d="M3.054,110.585s-20.841-66.647,31.834-119c39.747-39.5,111.923-37.591,141.994,5.689" transform="translate(1.013 36.68)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="5"/>
        </g>
        <g id="Group_170" data-name="Group 170" transform="translate(385.868 1395.829)">
            <path id="Path_349" data-name="Path 349" d="M220.921,167.38C187.385,265.512,39.565,309.258-38.568,208.208-69.507,168.191-79.512,125.63-69.583,66.856-57.856-2.581-2.166-55.794,82.468-55.517c38.081.124,79.623,12.406,107.611,47.09" transform="translate(73.277 55.518)" fill="none" stroke="#171410" stroke-linecap="round" stroke-width="8"/>
        </g>
        <path id="Path_350" data-name="Path 350" d="M0,256.526H817.922V-85.087H0Z" transform="translate(74.576 1471.054)" fill="none"/>
    </g>
</svg>

