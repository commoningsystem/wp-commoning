<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package commoning
 */

$commoning_description = get_bloginfo( 'description', 'display' );
$svg_path_logo_big     = get_template_directory_uri() . '/svg/logo-big.svg';
$svg_path_logo_small   = get_template_directory_uri() . '/svg/logo-small.svg';
$svg_path_arrow_right  = get_template_directory_uri() . '/svg/arrow-right.svg';

/**
 * the front page has a big header logo, which gets smaller when scrolled
 * this sets its height ond every viewport up of 'md'
 */
$additionalClasses = is_page_template( "page-app.php" ) ? 'lg:h-96' : '';
$additionalClasses2 = (!is_page_template( "page-aktiv.php" )) ? 'bg-white ' : '';
$additionalClasses3 = (!is_page_template( "page-aktiv.php" )) ? 'md:bg-white ' : '';

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class( 'bg-white' ); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site overflow-y-hidden">
    <a class="skip-link sr-only" href="#primary">
		<?php esc_html_e( 'Skip to content', 'commoning' ); ?>
    </a>

    <header id="masthead"
            class="site-header w-full  flex justify-between fixed top-0 bg-white z-20 lg:pl-0 lg:pr-0 lg:h-40">
        <div id="site-branding"
             class="site-branding flex ml-4 lg:ml-0 content-center lg:w-1/2 lg:pr-8 lg:pl-8 md:bg-white transition-height ease-out duration-200 <?php echo $additionalClasses ?>">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="zur Startseite"
               class="lg:w-auto bg-white flex" id="header-logo">
				<?php if ( is_front_page() ): ?>
                    <h1 class="site-title sr-only"><?php the_title(); ?></h1>
                    <img src="<?php echo $svg_path_logo_small ?>" alt="Commoning Logo mit Schriftzug"
                         id="header-logo-image-small" class="w-24 lg:w-48 ml-4 mr-4 hidden"/>
                    <img src="<?php echo $svg_path_logo_big ?>" alt="Commoning Logo mit Schriftzug"
                         class="hidden lg:block md:mt-10 md:mb-10"
                         id="header-logo-image-big-desktop"
                    >
				<?php else: ?>
					<?php if ( is_page_template(['page-app.php', 'page-aktiv.php', 'page-system.php']) ): ?>
                        <h1 class="site-title sr-only"><?php the_title(); ?></h1>
					<?php endif ?>
                    <img src="<?php echo $svg_path_logo_small ?>" alt="Commoning Logo mit Schriftzug"
                         id="header-logo-image-small" class="w-24 lg:w-48 ml-4 mr-4 "/>
				<?php endif; ?>
            </a>
			<?php if ( $commoning_description || is_customize_preview() ) : ?>
                <p class="site-description sr-only">
					<?php echo $commoning_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped	?>
                </p>
			<?php endif; ?>
        </div><!-- .site-branding -->
        <div class="nav-container flex-none w-28 lg:w-1/2 md:bg-red-100 md:flex">
            <label for="menu-toggle"
                   class="pointer-cursor lg:hidden block h-28 flex justify-center content-center items-center bg-black ">
                <svg viewBox="0 0 100 100" class="w-28 h-28 fill-none text-white stroke-2 stroke-current"
                     id="hamburger-icon">
                    <title>menu</title>
                    <path id="top-line-3" d="M30,37 L70,37 Z"></path>
                    <path id="middle-line-3" d="M30,50 L70,50 Z"></path>
                    <path id="bottom-line-3" d="M30,63 L70,63 Z"></path>
                </svg>
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle">
            <nav id="site-navigation"
                 class="main-navigation hidden lg:block w-full z-20 absolute lg:static top-28 lg:top-0 lg:pb-0 lg:pt-0 pt-24 pb-24 left-0 bg-white lg:h-40">
				<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'menu-header',
						'container_class' => 'h-screen lg:h-full',
						'menu_class'      => 'flex flex-col lg:flex-row justify-center items-center lg:h-full',
						// the ul
						'add_li_class'    => 'flex-1 w-full h-full',
						// every li
						'link_before'     => '<span class="self-end">',
						'link_after'      => '</span>',
						'menu_id'         => 'menu-header',
					)
				);
				?>
            </nav><!-- #site-navigation -->
        </div>
    </header><!-- #masthead -->
	<?php if ( is_front_page() ): ?>
        <div id="header-logo-image-big" class="w-full absolute top-8 z-10 bg-white pt-1 sm:pb-9 md:pb-9 pl-4 pr-4 lg:hidden">
            <img src="<?php echo $svg_path_logo_big ?>" alt="Commoning Logo mit Schriftzug"
                 class="">
        </div>
	<?php endif; ?>
