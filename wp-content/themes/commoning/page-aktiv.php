<?php
/*
* Template Name: Page: Aktiv
* Template Post Type: page
*/

get_header();
?>
    <main id="primary" class="site-main w-full text-2sm font-light lg:font-normal lg:text-base 2xl:text-2sm pt-40">
        <div class="flex flex-col ">
            <div class="w-full ">
                <div class="actors">
					<?php if ( get_field( 'actors_headline' ) ): ?>
                        <h1 class="lg:text-3xl font-bold mb-4">
							<?php the_field( 'actors_headline' ) ?>
                        </h1>
					<?php endif; ?>
					<?php if ( get_field( 'actors' ) ): ?>
						<?php the_field( 'actors' ) ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
		<?php
		/**
		 * DU möchtes sein Aufzählung aka main content
		 */
		?>
        <div class="flex flex-col bg-blau z-20 relative">
            <div class="w-full ">
                <div class="w-full lg:w-4/5">
					<?php if ( get_field( 'subheadline' ) ): ?>
                        <h2 class="text-white text-sm2 text-xl lg:text-base2"><?php the_field( 'subheadline' ); ?></h2>
					<?php endif; ?>
					<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'page-aktiv' );
					endwhile; // End of the loop.
					?>
					<?php if ( get_field( 'note_below_content' ) ): ?>
                        <p class="text-white text-sm font-bold"><?php the_field( 'note_below_content' ); ?></p>
					<?php endif; ?>
                </div>
            </div>
        </div>
		<?php
		/*
		 * Die Reihe unten mit den 2 Boxen
		 */
		?>
        <div class="flex flex-col lg:flex-row ">
            <div class="w-full lg:w-1/2 bg-black text-white well_find_us">
				<?php if ( get_field( 'contact_advice_headline' ) ): ?>
                    <h2 class="text-sm2 lg:text-base2 p-8 lg:pt-16 lg:pb-4  2xl:w-128 2xl:text-lg"><?php the_field( 'contact_advice_headline' ); ?></h2>
				<?php endif; ?>
                <div class="lg:mb-16 call-to-action relative pb-8 pl-8 pt-5 text-sm lg:pl-16 lg:pb-0 xl:pb-8">
                    <div class="w-96 text-base md:text-base lg:text-base md:w-100 2xl:text-lg xl:w-100 2xl:w-112 relative">
                        <div class="absolute -top-5 -left-10 transform scale-80 md:scale-100 lg:-top-1 md:-top-1 md:-left-8 lg:-left-11 lg:scale-100 xl:scale-100 xl:-left-11 xl:-top-1 2xl:-left-9 2xl:top-7 2xl:scale-150 z-0 ">
							<?php get_template_part( 'svg/bullet', 'wir-finden-uns.svg' ) ?>
                        </div>
						<?php if ( get_field( 'contact_advice_description' ) ): ?>
                            <p><?php the_field( 'contact_advice_description' ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>
			<?php // WENN DU LUST HAST, LIVE UND IN FARBE (GRÜNER KREIS) ?>

            <div id="call_to_action_einladung"
                 class="w-full bg-dunkelgruen lg:bg-gray-200 z-10 relative overflow-hidden">
                <div class="content p-8 2xl:p-16  z-10 flex items-center justify-between lg:absolute top-0 right-0 text-white text-base lg:text-xl lg:text-3xl 2xl:text-2xl lg:mb-6 2xl:mr-12 2xl:w-112 3xl:w-196  ">
                    <div class="circle absolute z-0 hidden lg:block" id="circle"></div>
                    <div class="z-10">
						<?php if ( get_field( 'invitation' ) ): ?>
                            <p><?php the_field( 'invitation' ); ?></p>
						<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_sidebar();
get_template_part( 'template-parts/modal', 'contact-form' );
get_footer();
