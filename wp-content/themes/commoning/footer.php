<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package commoning
 */
$front_page_id = get_option( 'page_on_front' );
?>

<footer id="colophon" class="site-footer flex-col bg-black z-10 relative">
    <div class="w-full bg-gelb flex-coll p-8 text-lg lg:text-4xl">
		<?php if ( get_field( 'footer_headline', $front_page_id ) ): ?>
            <h2 class="hidden lg:block text-4xl"><?php the_field( 'footer_headline', $front_page_id ); ?></h2>
		<?php endif; ?>
        <div class="w-full 2xl:mt-8">
			<?php if ( get_field( 'call_to_action_footer_1', $front_page_id ) ): ?>
				<?php
				$footer_cta_link_1     = get_field( 'call_to_action_footer_1', $front_page_id );
				if ( $footer_cta_link_1 ):
					$link_url_cta_1 = $footer_cta_link_1['url'];
					$link_title_cta_1  = $footer_cta_link_1['title'];
					$link_target_cta_1 = $footer_cta_link_1['target'] ?: '_self';
					?>
                    <a class="button group active:bg-black active:text-gelb p-2 lg:inline flex justify-between items-center"
                       href="<?php echo esc_url( $link_url_cta_1 ); ?>"
                       target="<?php echo esc_attr( $link_target_cta_1 ); ?>">
                        <span>
                        <?php echo esc_html( $link_title_cta_1 ); ?>
                        </span>
                        <span>
                        <?php get_template_part( 'svg/arrow', 'right.svg' ) ?>
                        </span>
                    </a>
				<?php endif; ?>
			<?php endif; ?>
        </div>
        <div class="w-full 2xl:mt-8">
			<?php if ( get_field( 'call_to_action_footer_2', $front_page_id ) ): ?>
				<?php
				$footer_cta_link_2     = get_field( 'call_to_action_footer_2', $front_page_id );
				if ( $footer_cta_link_2 ):
					$link_url_cta_2 = $footer_cta_link_2['url'];
					$link_title_cta_2  = $footer_cta_link_2['title'];
					$link_target_cta_2 = $footer_cta_link_2['target'] ?: '_self';
					?>
                    <a class="button group active:bg-black active:text-gelb p-2 lg:inline flex justify-between items-center"
                       href="<?php echo esc_url( $link_url_cta_2 ); ?>"
                       target="<?php echo esc_attr( $link_target_cta_2 ); ?>">
                        <span>
						<?php echo esc_html( $link_title_cta_2 ); ?>
                        </span>
                        <span>
						<?php get_template_part( 'svg/arrow', 'right.svg' ) ?>
                        </span>
                    </a>
				<?php endif; ?>
			<?php endif; ?>
        </div>
    </div>
    <div class="flex flex-col-reverse md:flex-row">
        <nav class="secondary-navigation w-full md:w-1/2 text-white p-8 pt-10 text-base">
            <button class="menu-toggle hidden" aria-controls="menu-footer"
                    aria-expanded="false"><?php esc_html_e( 'Secondary Menu', 'commoning' ); ?></button>
			<?php $location_left = "menu-footer"; ?>
            <div class="italic mb-5">
				<?php echo wp_get_nav_menu_name( $location_left ) ?>:
            </div>
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => $location_left,
					'container_class' => 'h-full',
					'add_li_class'    => 'mb-4', //put only layouty classes, here
				)
			);
			?>
        </nav>
        <nav class="social-navigation w-full md:w-1/2 text-white p-8 pt-10 text-base text-right">
            <button class="menu-toggle hidden" aria-controls="menu-footer"
                    aria-expanded="false"><?php esc_html_e( 'Social Menu', 'commoning' ); ?></button>
			<?php $location_right = "menu-footer-external-links"; ?>
            <div class="italic mb-5">
				<?php echo wp_get_nav_menu_name( $location_right ) ?>:
            </div>
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => $location_right,
					'container_class' => 'h-full',
					'add_li_class'    => 'mb-4', //put only layouty classes, here
				)
			);
			?>
        </nav>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
